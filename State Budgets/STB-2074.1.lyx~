#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation skip
\defskip medskip
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Supplementary Federal Budget of Stutzland 2074
\end_layout

\begin_layout Date
1st of July 2074
\end_layout

\begin_layout Standard
\align center
\begin_inset Graphics
	filename ../../../../Pictures/st_vaakuna.png
	lyxscale 20
	scale 10

\end_inset


\end_layout

\begin_layout Standard
This is a revised budget for the Federal State for the year 2074, as the
 previous budget was heavily uninformed and lacking in scale in terms of
 both income and expenses.
\end_layout

\begin_layout Standard
The information presented in this budget is not to be added on top of the
 previous budget, but to replace it in total.
\end_layout

\begin_layout Section
State income
\end_layout

\begin_layout Standard
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="10" columns="3">
<features tabularvalignment="middle">
<column alignment="left" valignment="top">
<column alignment="right" valignment="top">
<column alignment="right" valignment="top">
<row>
<cell alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Type of income
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Amount / percentage
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Brute income (€)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Valued-Added-Tax (VAT)
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
20,00%
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
11 910 407
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Private Profit Tax (PPT)
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
18,76%
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
7 698 972
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Personal Tax (PET)
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
18,76% 
\emph on
(effective)
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
70 282 230
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Vehicle Tax (VET)
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
116 €
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
100 641
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Healthcare Insurance Payments
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1,05%
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3 933 142
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Pension Insurance Payments
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1,05%
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3 933 142
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Federal investments
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
10,00%
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
4 103 930
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Federal trade deals
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
-
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2 000 000
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
103 962 463
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Section
State expenses
\end_layout

\begin_layout Standard
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="10" columns="3">
<features tabularvalignment="middle">
<column alignment="left" valignment="top">
<column alignment="right" valignment="top">
<column alignment="right" valignment="top">
<row>
<cell alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Ministry
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Expenses (€)
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
%
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Reichskanzlei
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
4 615 048
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
4,29
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Ministry of Finances
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
50 467 829
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
46,92
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Ministry of Foreign Affairs
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1 465 780
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
1,36
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Ministry of Security
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
4 165 453
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
3,87
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Ministry of Armed Forces
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
16 904 669
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
15,72
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Ministry of Welfare
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
23 880 776
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
22,20
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Ministry of Education
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
5 053 032
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
4,70
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Ministry of Infrastructure
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
998 599
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
0,93
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
107 551 186
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
100,00
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Section
Budget balance
\end_layout

\begin_layout Standard
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="3" columns="2">
<features tabularvalignment="middle">
<column alignment="left" valignment="top">
<column alignment="right" valignment="top">
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Income (€)
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
103 962 463
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Expenses (€)
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
107 551 186
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Total (€)
\end_layout

\end_inset
</cell>
<cell alignment="right" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
─ 3 588 723
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard
The State Budget for the year 2074 will have a deficit of 3 588 723 €.
 The deficit will be covered with domestic loans.
 Expected actual interest rate for year 2074 is 1,86 %.
\end_layout

\end_body
\end_document
